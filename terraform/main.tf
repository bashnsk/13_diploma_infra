terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
    token                    = var.YC_TOKEN
    cloud_id                 = var.YC_CLOUD_ID
    folder_id                = var.YC_FOLDER_ID
    zone = var.ZONE
}


# resource "acme_certificate" "certificate" {

#   dns_challenge {
#     provider = "regru"
#   }
# }

# provider "regru" {
#   password = var.REG_RU_PASSWORD
#   username = var.REG_RU_USERNAME
# }

# resource "regru_dns_zone" "site-url" {
#   domain  = "surikov.site"
# }

# resource "regru_dns_zone_record" "a_a" {
#   zone = regru_dns_zone.site-url.domain
#   host            = "A"
#   type            = "TXT"
#   value           = [for listener in yandex_lb_network_load_balancer.lb.listener :
#    [for external_address_spec in listener.external_address_spec :
#      external_address_spec.ip_version == "ipv4" ? external_address_spec.address : null
#    ]
#   ]    #"158.160.40.207"
#   ttl             = 10
#   external_id     = ""
#   additional_info = ""
# }