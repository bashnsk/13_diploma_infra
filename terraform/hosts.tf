// Create hosts file and copy content to ansible_path
resource "null_resource" "copy_hosts" {
  provisioner "local-exec" {
    command = "echo '${local_file.inventory.content}' > ../ansible/hosts && cp -f ../ansible/hosts ../../service/ansible/"
    interpreter = ["/bin/bash", "-c"] 
}
    # Гарантируем, что ресурс `copy_hosts` 
    # будет выполнен только после ресурса `local_file.inventory
    depends_on = [local_file.inventory]
}


# Создаем инветори-файл для Ансибла
resource "local_file" "inventory" {
  filename = "hosts"
  content = <<EOF
[localhost]
127.0.0.1 ansible_connection=local

[proxy_domain]
proxy ansible_host=51.250.54.180 ansible_user=ubuntu ansible_ssh_private_key_file=~/.ssh/id_rsa.pub

[load_balancer_stage]
load-balancer-ip=${tostring(element(local.load_balancer_ips_stage[0], 0))}

[load_balancer_prod]
load-balancer-ip=${tostring(element(local.load_balancer_ips_prod[0], 0))}

[ci_server]
ci-server ansible_host=${yandex_compute_instance.ci.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key_file=${var.RSA_KEY_PATH}

[cd_server]
cd-server ansible_host=${yandex_compute_instance.cd.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key_file=${var.RSA_KEY_PATH}

[monitoring]
monitoring-server ansible_host=${yandex_compute_instance.ms.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key_file=${var.RSA_KEY_PATH}

[stage_servers]
stage_server_1 ansible_host=${yandex_compute_instance_group.ig_stage.instances[0].network_interface[0].nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key_file=${var.RSA_KEY_PATH}
stage_server_2 ansible_host=${yandex_compute_instance_group.ig_stage.instances[1].network_interface[0].nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key_file=${var.RSA_KEY_PATH}

[prod_servers]
prod_server_1 ansible_host=${yandex_compute_instance_group.ig_prod.instances[0].network_interface[0].nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key_file=${var.RSA_KEY_PATH}
prod_server_2 ansible_host=${yandex_compute_instance_group.ig_prod.instances[1].network_interface[0].nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key_file=${var.RSA_KEY_PATH}
EOF
}

locals {
load_balancer_ips_stage = [for listener in yandex_lb_network_load_balancer.lb_stage.listener : flatten([
for external_address_spec in listener.external_address_spec :
external_address_spec.ip_version == "ipv4" ? external_address_spec.address : null
])]
}

locals {
load_balancer_ips_prod = [for listener in yandex_lb_network_load_balancer.lb_prod.listener : flatten([
for external_address_spec in listener.external_address_spec :
external_address_spec.ip_version == "ipv4" ? external_address_spec.address : null
])]
}