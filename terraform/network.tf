// Configure VPC Nework
resource "yandex_vpc_network" "vpc-network" {
  name = "vpc-network"
}
resource "yandex_vpc_subnet" "vpc-subnet" {
  name           = "vpc-subnet"
  zone           = var.ZONE
  network_id     = "${yandex_vpc_network.vpc-network.id}"
  v4_cidr_blocks = ["10.241.1.0/24"]
}
