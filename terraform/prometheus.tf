# Создаем конфиг с адресами машин
resource "local_file" "prometheus" {
  filename = "prometheus.conf"
  content = <<EOF
global:
  scrape_interval: 15s 
  evaluation_interval: 15s 

alerting:
  alertmanagers:
    - static_configs:
        - targets:
          - "'${yandex_compute_instance.ms.network_interface.0.nat_ip_address}':9093"

rule_files:
  - "alertrules.yml"
scrape_configs:

  - job_name: "monitoring"
    static_configs:
      - targets: 
        - "'${yandex_compute_instance.ms.network_interface.0.nat_ip_address}':9100"

  - job_name: "ci"
    static_configs:
      - targets: 
        - "'${yandex_compute_instance.ci.network_interface.0.nat_ip_address}':8080"
        - "'${yandex_compute_instance.ci.network_interface.0.nat_ip_address}':9100"

  - job_name: "cd"
    static_configs:
      - targets: 
        - "'${yandex_compute_instance.cd.network_interface.0.nat_ip_address}':8080"
        - "'${yandex_compute_instance.cd.network_interface.0.nat_ip_address}':9100"

  - job_name: "stage-1"
    static_configs:
      - targets: 
        - "'${yandex_compute_instance_group.ig_stage.instances[0].network_interface[0].nat_ip_address}':8080"
        - "'${yandex_compute_instance_group.ig_stage.instances[0].network_interface[0].nat_ip_address}':9100"

  - job_name: "stage-2"
    static_configs:
      - targets: 
        - "'${yandex_compute_instance_group.ig_stage.instances[1].network_interface[0].nat_ip_address}':8080"
        - "'${yandex_compute_instance_group.ig_stage.instances[1].network_interface[0].nat_ip_address}':9100"

  - job_name: "prod-1"
    static_configs:
      - targets: 
        - "'${yandex_compute_instance_group.ig_prod.instances[0].network_interface[0].nat_ip_address}':8080"
        - "'${yandex_compute_instance_group.ig_prod.instances[0].network_interface[0].nat_ip_address}':9100"

  - job_name: "prod-2"
    static_configs:
      - targets: 
        - "'${yandex_compute_instance_group.ig_prod.instances[1].network_interface[0].nat_ip_address}':8080"
        - "'${yandex_compute_instance_group.ig_prod.instances[1].network_interface[0].nat_ip_address}':9100"
EOF

  # Копируем содержимое 
  provisioner "local-exec" {
    command = "echo '${local_file.prometheus.content}' > ../ansible/roles/config-monitoring/files/prometheus.conf"
    interpreter = ["/bin/bash", "-c"]
}
depends_on = [local_file.inventory]
}