variable "REG_RU_USERNAME" {
    description = "Reg.ru username"
    default     = "" # load from tfvars file
    type = string
    sensitive = true
}
variable "REG_RU_PASSWORD" {
    description = "Reg.ru password"
    default     = "" # load from tfvars file
    type = string
    sensitive = true
}
variable "YC_TOKEN" {
    description = "Yandex Cloud security OAuth token"
    default     = "" # load from tfvars file
    type = string
    sensitive = true
}
variable "YC_CLOUD_ID" {
    description = "Yandex Cloud ID where resources will be created"
    default     = "" # load from tfvars file
    type = string
    sensitive = true
}
variable "YC_FOLDER_ID" {
    description = "Yandex Cloud Folder ID where resources will be created"
    default     = "" #load from tfvars file
    type = string
    sensitive = true
}
variable "YC_SERVICE_ACCOUNT" {
    description = "Yandex Cloud Service account"
    default     = "" #load from tfvars file
    type = string
    sensitive = true
}
variable "ZONE" {
    description = "Zone"
    default     = "ru-central1-a"
    type = string
}
variable "RSA_KEY_PATH" {
    description = "Path and ssh-key"
    default     = "~/.ssh/id_rsa.pub"
    type = string
}
variable "BOOT_IMAGE_ID" {
    description = "Boot image ID"
    default     = "fd8fhotkbjr93oltt3d3"
    type = string
}
variable "PLATFORM_ID" {
    description = "Platform"
    default     = "standard-v2"
    type = string
}
variable "PREEMPTIBLE" {
    description = "Resource interrupt capability"
    default     = "true"
    type = string
}
variable "BOOT_DISK_TYPE" {
    description = "Boot disk type"
    default     = "network-hdd"
    type = string
}
variable "BOOT_DISK_SIZE" {
    description = "Boot disk size"
    default     = 15
    type = number
}
variable "RESOURCES_CORE_FRACTION" {
    description = "Resources core fraction"
    default     = 20
    type = number
}
variable "RESOURCES_CORES" {
    description = "Resources cores"
    default     = 2
    type = number
}
variable "RESOURCES_MEMORY" {
    description = "Resources memory"
    default     = 1
    type = number
}
