// Show external IP
output "Stage_server_1_external_IP" {
  value = "${yandex_compute_instance_group.ig_stage.instances[0].network_interface[0].nat_ip_address}"
}
output "Stage_server_2_external_IP" {
  value = "${yandex_compute_instance_group.ig_stage.instances[1].network_interface[0].nat_ip_address}"
}

output "Prod_server_1_external_IP" {
  value = "${yandex_compute_instance_group.ig_prod.instances[0].network_interface[0].nat_ip_address}"
}
output "Prod_server_2_external_IP" {
  value = "${yandex_compute_instance_group.ig_prod.instances[1].network_interface[0].nat_ip_address}"
}

output "CI_server_IP"{
  value = "${yandex_compute_instance.ci.network_interface.0.nat_ip_address}"
}

output "CD_server_IP"{
  value = "${yandex_compute_instance.cd.network_interface.0.nat_ip_address}"
}

output "Monitoring_server_IP"{
  value = "${yandex_compute_instance.ms.network_interface.0.nat_ip_address}"
}

output "Stage_load_balancer_ip" {
value = join(", ", distinct(local.load_balancer_ips_stage[0]))
}

output "Prod_load_balancer_ip" {
value = join(", ", distinct(local.load_balancer_ips_prod[0]))
}

# output "load_balancer_address" {
#   value = [
#     for listener in yandex_lb_network_load_balancer.lb.listener :
#     [for external_address_spec in listener.external_address_spec :
#       external_address_spec.ip_version == "ipv4" ? external_address_spec.address : null
#     ]
#   ]
#   }


# output "load_balancer_ip" {
# value = join(", ", [for addresses in yandex_lb_network_load_balancer.lb.listener[*].external_address_spec[*] : addresses.ip_version == "ipv4" ? addresses.address : null])
# }

# Inventory for Ansible
output "inventory_content" {
  value = local_file.inventory.content
}

