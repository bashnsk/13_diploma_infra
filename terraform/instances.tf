
# // Create a new instance
# resource "yandex_compute_instance" "instance-1" {
#   name = "instance-1"
#   folder_id = var.YC_FOLDER_ID
#   platform_id = var.PLATFORM_ID
#   zone = var.ZONE

#   scheduling_policy {
#     preemptible = var.PREEMPTIBLE
#   }

#   resources {
#     core_fraction = var.RESOURCES_CORE_FRACTION
#     cores = var.RESOURCES_CORES
#     memory = var.RESOURCES_MEMORY
#   }

#   boot_disk {
#     initialize_params {
#       image_id = var.BOOT_IMAGE_ID
#       type = var.BOOT_DISK_TYPE
#       size = var.BOOT_DISK_SIZE
#     }
#   }

#   network_interface {
#     subnet_id = yandex_vpc_subnet.vpc-subnet.id
#     nat = true
#   }

#   metadata = {
#     ssh-keys = "ubuntu:${file(var.RSA_KEY_PATH)}"
#   }
# }


// Create a stage instance group
resource "yandex_compute_instance_group" "ig_stage" {
  name                = "instance-group-stage"
  folder_id           = var.YC_FOLDER_ID
  service_account_id  = var.YC_SERVICE_ACCOUNT
  instance_template {
    platform_id = var.PLATFORM_ID
    resources {
      core_fraction = var.RESOURCES_CORE_FRACTION
      memory = var.RESOURCES_MEMORY
      cores  = var.RESOURCES_CORES
    }
    boot_disk {
      initialize_params {
        image_id = var.BOOT_IMAGE_ID
        type = var.BOOT_DISK_TYPE
        size = var.BOOT_DISK_SIZE
      }
    }
    network_interface {
      network_id = "${yandex_vpc_network.vpc-network.id}"
      subnet_ids = ["${yandex_vpc_subnet.vpc-subnet.id}"]
      nat = true
    }
    metadata = {
      ssh-keys = "ubuntu:${file(var.RSA_KEY_PATH)}"
    }
  }

  scale_policy {
    fixed_scale {
      size = 2
    }
  }

  allocation_policy {
    zones = [var.ZONE]
  }

  deploy_policy {
    max_unavailable = 1
    max_expansion   = 0
  }

  load_balancer {
    target_group_name        = "lbsg"
    target_group_description = "load balancer stage group"
  }

}



// Configure stage group load balancer 
resource "yandex_lb_network_load_balancer" "lb_stage" {
  name = "network-load-balancer-stage"

  listener {
    name = "listener"
    port = 8080
    external_address_spec {
        # address = "158.160.40.207"
        ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = "${yandex_compute_instance_group.ig_stage.load_balancer.0.target_group_id}"
    healthcheck {
      name = "http"
      http_options {
        port = 8080
        path = "/"
      }
    }
  }
}

// Create a prod instance group
resource "yandex_compute_instance_group" "ig_prod" {
  name                = "instance-group-prod"
  folder_id           = var.YC_FOLDER_ID
  service_account_id  = var.YC_SERVICE_ACCOUNT
  instance_template {
    platform_id = var.PLATFORM_ID
    resources {
      core_fraction = var.RESOURCES_CORE_FRACTION
      memory = var.RESOURCES_MEMORY
      cores  = var.RESOURCES_CORES
    }
    boot_disk {
      initialize_params {
        image_id = var.BOOT_IMAGE_ID
        type = var.BOOT_DISK_TYPE
        size = var.BOOT_DISK_SIZE
      }
    }
    network_interface {
      network_id = "${yandex_vpc_network.vpc-network.id}"
      subnet_ids = ["${yandex_vpc_subnet.vpc-subnet.id}"]
      nat = true
    }
    metadata = {
      ssh-keys = "ubuntu:${file(var.RSA_KEY_PATH)}"
    }
  }

  scale_policy {
    fixed_scale {
      size = 2
    }
  }

  allocation_policy {
    zones = [var.ZONE]
  }

  deploy_policy {
    max_unavailable = 1
    max_expansion   = 0
  }

  load_balancer {
    target_group_name        = "lbpg"
    target_group_description = "load balancer prod group"
  }

}



// Configure prod load balancer
resource "yandex_lb_network_load_balancer" "lb_prod" {
  name = "network-load-balancer-prod"

  listener {
    name = "listener"
    port = 8080
    external_address_spec {
        # address = "158.160.40.207"
        ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = "${yandex_compute_instance_group.ig_prod.load_balancer.0.target_group_id}"
    healthcheck {
      name = "http"
      http_options {
        port = 8080
        path = "/"
      }
    }
  }
}

// Create a CI server instance
resource "yandex_compute_instance" "ci" {
  name = "ci-server"
  folder_id           = var.YC_FOLDER_ID
  service_account_id  = var.YC_SERVICE_ACCOUNT
  platform_id = var.PLATFORM_ID
  zone = var.ZONE
  scheduling_policy {
    preemptible = var.PREEMPTIBLE
  }

  resources {
    core_fraction = var.RESOURCES_CORE_FRACTION
    cores = var.RESOURCES_CORES
    memory = var.RESOURCES_MEMORY
  }

  boot_disk {
    initialize_params {
      image_id = var.BOOT_IMAGE_ID
      type = var.BOOT_DISK_TYPE
      size = var.BOOT_DISK_SIZE
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.vpc-subnet.id
    nat = true
  }
  metadata = {
    ssh-keys = "ubuntu:${file(var.RSA_KEY_PATH)}"
  }
}

// Create a CD server instance
resource "yandex_compute_instance" "cd" {
  name = "cd-server"
  folder_id           = var.YC_FOLDER_ID
  service_account_id  = var.YC_SERVICE_ACCOUNT
  platform_id = var.PLATFORM_ID
  zone = var.ZONE
  scheduling_policy {
    preemptible = var.PREEMPTIBLE
  }

  resources {
    core_fraction = var.RESOURCES_CORE_FRACTION
    cores = var.RESOURCES_CORES
    memory = var.RESOURCES_MEMORY
  }

  boot_disk {
    initialize_params {
      image_id = var.BOOT_IMAGE_ID
      type = var.BOOT_DISK_TYPE
      size = var.BOOT_DISK_SIZE
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.vpc-subnet.id
    nat = true
  }
  metadata = {
    ssh-keys = "ubuntu:${file(var.RSA_KEY_PATH)}"
  }
}

// Create a monitoring server instance
resource "yandex_compute_instance" "ms" {
  name = "monitoring-server"
  folder_id           = var.YC_FOLDER_ID
  service_account_id  = var.YC_SERVICE_ACCOUNT
  platform_id = var.PLATFORM_ID
  zone = var.ZONE
  scheduling_policy {
    preemptible = var.PREEMPTIBLE
  }

  resources {
    core_fraction = var.RESOURCES_CORE_FRACTION
    cores = var.RESOURCES_CORES
    memory = var.RESOURCES_MEMORY
  }

  boot_disk {
    initialize_params {
      image_id = var.BOOT_IMAGE_ID
      type = var.BOOT_DISK_TYPE
      size = var.BOOT_DISK_SIZE
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.vpc-subnet.id
    nat = true
  }
  metadata = {
    ssh-keys = "ubuntu:${file(var.RSA_KEY_PATH)}"
  }
}