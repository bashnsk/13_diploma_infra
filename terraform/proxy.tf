# Создаем проброс ip-шек для прокси
resource "local_file" "proxy-file" {
  filename = "proxy-nginx-enabled-file"
  content = <<EOF
server {
  listen 8080;
  server_name surikov.site;

  location / {
  proxy_pass http://${tostring(element(local.load_balancer_ips_prod[0], 0))}:8080;
}
}
  server {
  listen 8080;
  server_name stage.surikov.site;

  location / {
  proxy_pass http://${tostring(element(local.load_balancer_ips_stage[0], 0))}:8080;
proxy_set_header Host $host;
proxy_set_header X-Real-IP $remote_addr;
}
}

server {
  listen 8080;
  server_name dev.surikov.site;

  location / {
  proxy_pass http://${yandex_compute_instance.ci.network_interface.0.nat_ip_address}:8080;

proxy_set_header Host $host;
proxy_set_header X-Real-IP $remote_addr;
}
}

server {
  listen 8080;
  server_name test.surikov.site;

  location / {
  proxy_pass http://${yandex_compute_instance.cd.network_interface.0.nat_ip_address}:8080;

proxy_set_header Host $host;
proxy_set_header X-Real-IP $remote_addr;
}
}

server {
  listen 9090;
  server_name monitoring.surikov.site;

  location / {
  proxy_pass http://${yandex_compute_instance.ms.network_interface.0.nat_ip_address}:9090;

proxy_set_header Host $host;
proxy_set_header X-Real-IP $remote_addr;
}
}

server {
  listen 3000;
  server_name grafana.surikov.site;

  location / {
  proxy_pass http://${yandex_compute_instance.ms.network_interface.0.nat_ip_address}:3000;

proxy_set_header Host $host;
proxy_set_header X-Real-IP $remote_addr;
}
}
EOF

  # Копируем содержимое 
  provisioner "local-exec" {
    command = "echo '${local_file.proxy-file.content}' > ../ansible/proxy-nginx-enabled-file"
    interpreter = ["/bin/bash", "-c"]
}
depends_on = [local_file.inventory]
}